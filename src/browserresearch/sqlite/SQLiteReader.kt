package browserresearch.sqlite

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet

class SQLiteReader {
    private var connection: Connection? = null

    fun connect(path: String) {
        Class.forName("org.sqlite.JDBC")
        connection = DriverManager.getConnection("jdbc:sqlite:" + path)
        connection?.isReadOnly = true
    }

    fun selectAllFromTable(table: String): ResultSet? {
        return executeQuery("select * from $table")
    }

    fun executeQuery(query: String): ResultSet? {
        return connection?.createStatement()?.executeQuery(query)
    }

    fun close() {
        connection?.close()
    }
}
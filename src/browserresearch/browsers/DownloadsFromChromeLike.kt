package browserresearch.browsers

import browserresearch.sqlite.SQLiteReader
import java.text.SimpleDateFormat
import java.util.*

class DownloadsFromChromeLike() {

    private val downloadsTable = "downloads"

    fun getDownloads(historyPath: String): Array<Array<String>> {
        val table = mutableListOf(arrayOf(
            "id загрузки",
            "Текущий путь",
            "Время начала",
            "Время окончания",
            "Загружено байт",
            "Всего байт",
            "Ссылка"
        ))
        val format = SimpleDateFormat("HH:mm dd.MM.yy", Locale.getDefault())
        val sqlite = SQLiteReader()
        sqlite.connect(historyPath)
        val resultSet = sqlite.selectAllFromTable(downloadsTable)
        resultSet?.let { rs ->
            while (rs.next()) {
                val downloadId = rs.getLong("id").toString()
                val currentPath = rs.getString("current_path").toString()
                val startTime = rs.getLong("start_time")
                val endTime = rs.getLong("end_time")
                val receivedBytes = rs.getLong("received_bytes").toString()
                val totalBytes = rs.getLong("total_bytes").toString()
                val tabUrl = rs.getString("tab_url")
                val startTimeString = format.format(startTime)
                val endTimeString = format.format(endTime)
                table.add(
                    arrayOf(downloadId, currentPath, startTimeString, endTimeString, receivedBytes, totalBytes, tabUrl)
                )
            }
        }
        sqlite.close()
        return table.toTypedArray()
    }
}
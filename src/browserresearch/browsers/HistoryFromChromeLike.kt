package browserresearch.browsers

import browserresearch.sqlite.SQLiteReader
import java.text.SimpleDateFormat
import java.util.*


class HistoryFromChromeLike() {

    private val historyQuery = "select " +
            "visits.id as 'id_visit', " +
            "urls.id as 'id_url', " +
            "urls.url, " +
            "urls.title, " +
            "datetime(visit_time / 1000000 + (strftime('%s', '1601-01-01')), 'unixepoch') as 'visit_time'" +
            "from urls inner join visits on urls.id = visits.url"

    fun getHistory(historyPath: String): Array<Array<String>> {
        val table = mutableListOf(
            arrayOf(
                "id visit",
                "id url",
                "URL",
                "Заголовок",
                "Время посещения"
            )
        )
        val formatUTC = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        formatUTC.timeZone = TimeZone.getTimeZone("UTC")
        val format = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault())
        val sqlite = SQLiteReader()
        sqlite.connect(historyPath)
        val resultSet = sqlite.executeQuery(historyQuery)
        resultSet?.let { rs ->
            while (rs.next()) {
                val idVisit = rs.getLong("id_visit").toString()
                val idUrl = rs.getLong("id_url").toString()
                val url = rs.getString("url")
                val title = rs.getString("title")
                val dateUTC = rs.getString("visit_time")
                val date = formatUTC.parse(dateUTC)
                val dateLocal = format.format(date)

                table.add(arrayOf(idVisit, idUrl, url, title, dateLocal))
            }
        }
        sqlite.close()
        return table.toTypedArray()
    }
}
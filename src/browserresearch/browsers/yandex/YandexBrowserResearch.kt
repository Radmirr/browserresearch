package browserresearch.browsers.yandex

import browserresearch.browsers.*
import browserresearch.filesystem.FileSystemSeparator
import browserresearch.registry.ForensicRegistry
import browserresearch.registry.rejistry.TypeOS
import java.io.File

class YandexBrowserResearch(
    val initialPath: String
): BrowserResearch {

    private val s = FileSystemSeparator.getSeparator()
    private val usersPath = File(initialPath, "Users")
    private val browserPathWin10 = "${s}AppData${s}Local${s}Yandex${s}YandexBrowser${s}User Data${s}Default${s}"
    private val browserPathWinXP = "${s}AppData${s}Local${s}Yandex${s}YandexBrowser${s}User Data${s}Default${s}"

    private val historyFile = "History"

    private var users: List<UserData>

    init {
        val typeOS = try {
            ForensicRegistry(initialPath).getTypeOS()
        } catch (e: Exception) {
            TypeOS.WINDOWS_10
        }
        val browserPath = when (typeOS) {
            TypeOS.WINDOWS_XP -> browserPathWinXP
            else -> browserPathWin10
        }
        val usersFolder = usersPath
        val userNames = usersFolder.list({ dir, name -> File(dir, name).isDirectory })
        this.users = userNames
            .map { UserData(it, usersPath.path + s + it + browserPath) }
            .filter { File(it.browserPath).exists() }
    }

    override fun getBrowserName(): String = "Яндекс.Браузер"

    override fun getBrowserData(): List<BrowserResult> {
        val historyGetter = HistoryFromChromeLike()
        val searchTermsGetter = SearchTermsFromChromeLike()
        val downloadsGetter = DownloadsFromChromeLike()
        return users.map {
            val urlsData = historyGetter.getHistory(it.browserPath + historyFile)
            val termData = searchTermsGetter.getSearchTerms(it.browserPath + historyFile)
            val downloadsData = downloadsGetter.getDownloads(it.browserPath + historyFile)
            val urlsTable = BrowserTable("history", urlsData)
            val termsTable = BrowserTable("search terms", termData)
            val downloadsTable = BrowserTable("downloads", downloadsData)
            BrowserResult(it.userName, arrayOf(
                urlsTable,
                termsTable,
                downloadsTable
            ))
        }
    }
}
package browserresearch.browsers

class BrowserResult(
    val name: String,
    val tables: Array<BrowserTable>
)

class BrowserTable(
    val name: String,
    val data: Array<Array<String>>
)
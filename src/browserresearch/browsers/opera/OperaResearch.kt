package browserresearch.browsers.opera

import browserresearch.browsers.*
import browserresearch.filesystem.FileSystemSeparator
import browserresearch.registry.ForensicRegistry
import browserresearch.registry.rejistry.TypeOS
import java.io.File

class OperaResearch(
    val initialPath: String
): BrowserResearch {

    private val s = FileSystemSeparator.getSeparator()
    private val usersPath = File(initialPath, "Users")
    private val browserPathWin10 = "${s}AppData${s}Roaming${s}Opera Software${s}Opera Stable${s}"
    private val browserPathWinXP = "${s}AppData${s}Roaming${s}Opera Software${s}Opera Stable${s}"

    private val historyFile = "History"

    private var users: List<UserData>

    init {
        val typeOS = try {
            ForensicRegistry(initialPath).getTypeOS()
        } catch (e: Exception) {
            TypeOS.WINDOWS_10
        }
        val browserPath = when (typeOS) {
            TypeOS.WINDOWS_XP -> browserPathWinXP
            else -> browserPathWin10
        }
        val usersFolder = usersPath
        val userNames = usersFolder.list({ dir, name -> File(dir, name).isDirectory })
        this.users = userNames
            .map { UserData(it, usersPath.path + s + it + browserPath) }
            .filter { File(it.browserPath).exists() }
    }

    override fun getBrowserName(): String = "Opera"

    override fun getBrowserData(): List<BrowserResult> {
        val historyGetter = HistoryFromChromeLike()
        val searchTermsGetter = SearchTermsFromChromeLike()
        val downloadsGetter = DownloadsFromChromeLike()
        return users.map {
            val urlsData = historyGetter.getHistory(it.browserPath + historyFile)
            val termData = searchTermsGetter.getSearchTerms(it.browserPath + historyFile)
            val downloadsData = downloadsGetter.getDownloads(it.browserPath + historyFile)
            val urlsTable = BrowserTable("history", urlsData)
            val termsTable = BrowserTable("search terms", termData)
            val downloadsTable = BrowserTable("downloads", downloadsData)
            BrowserResult(it.userName, arrayOf(
                urlsTable,
                termsTable,
                downloadsTable
            ))
        }
    }
}
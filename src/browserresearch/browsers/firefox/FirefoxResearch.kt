package browserresearch.browsers.firefox

import browserresearch.browsers.BrowserResearch
import browserresearch.browsers.BrowserResult
import browserresearch.browsers.BrowserTable
import browserresearch.browsers.UserData
import browserresearch.filesystem.FileSystemSeparator
import browserresearch.registry.ForensicRegistry
import browserresearch.registry.rejistry.TypeOS
import browserresearch.sqlite.SQLiteReader
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class FirefoxResearch(
    val initialPath: String
) : BrowserResearch {

    private val s = FileSystemSeparator.getSeparator()
    private val usersPath = File(initialPath, "Users")
    private val pathToProfilesWin10 = "${s}AppData${s}Roaming${s}Mozilla${s}Firefox${s}Profiles"
    private val pathToProfilesWinXp = "${s}Application Data${s}Mozilla${s}Firefox${s}Profiles"

    private val historyFile = "places.sqlite"

    private var users: List<UserData>

    private val historyQuery = "select " +
            "moz_historyvisits.id as 'history_id', " +
            "moz_places.id as 'place_id', " +
            "url, " +
            "title, " +
            "visit_date " +
            "from moz_places inner join moz_historyvisits on moz_places.id == moz_historyvisits.place_id"

    init {
        val typeOS = try {
            ForensicRegistry(initialPath).getTypeOS()
        } catch (e: Exception) {
            TypeOS.WINDOWS_10
        }
        val profilesPath = when (typeOS) {
            TypeOS.WINDOWS_XP -> pathToProfilesWinXp
            else -> pathToProfilesWin10
        }
        val usersFolder = usersPath
        val userNames = usersFolder.list({ dir, name -> File(dir, name).isDirectory })
        users = userNames
            .mapNotNull { userName ->
                val profilesPath1 = File(usersPath.path + userName + profilesPath)
                if (profilesPath1.exists()) {
                    val profiles = profilesPath1.list { _, name -> name.contains(".default") }
                    profiles.map {
                        UserData(
                            userName + " " + it,
                            profilesPath1.absolutePath + "${s}" + it
                        )
                    }
                } else null
            }
            .reduce { acc, list -> acc.plus(list) }
    }

    override fun getBrowserName(): String = "Firefox"

    override fun getBrowserData(): List<BrowserResult> = users.map {
        val history = BrowserTable("history", getHistory(it.browserPath + s + historyFile))
        BrowserResult(
            it.userName,
            arrayOf(
                history
            )
        )
    }

    fun getHistory(historyPath: String): Array<Array<String>> {
        val table = mutableListOf(
            arrayOf(
                "id history",
                "id place",
                "URL",
                "Заголовок",
                "Последнее время посещения"
            )
        )
        val format = SimpleDateFormat("HH:mm dd.MM.yy", Locale.getDefault())
        val sqlite = SQLiteReader()
        sqlite.connect(historyPath)
        val resultSet = sqlite.executeQuery(historyQuery)
        resultSet?.let { rs ->
            while (rs.next()) {
                val historyId = rs.getLong("history_id").toString()
                val placeId = rs.getLong("place_id").toString()
                val url = rs.getString("url")
                val title = rs.getString("title")
                val visitTimestamp = rs.getLong("last_visit_date")
                val time = Date(visitTimestamp)
                val visitTime = format.format(time)
                table.add(arrayOf(historyId, placeId, url, title, visitTime))
            }
        }
        sqlite.close()
        return table.toTypedArray()
    }
}
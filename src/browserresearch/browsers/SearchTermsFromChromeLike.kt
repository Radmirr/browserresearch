package browserresearch.browsers

import browserresearch.sqlite.SQLiteReader

class SearchTermsFromChromeLike() {

    private val keyword_table = "keyword_search_terms"

    fun getSearchTerms(historyPath: String): Array<Array<String>> {
        val table = mutableListOf(arrayOf(
            "id keyword",
            "id url",
            "Поисковый запрос"
        ))
        val sqlite = SQLiteReader()
        sqlite.connect(historyPath)
        val resultSet = sqlite.selectAllFromTable(keyword_table)
        resultSet?.let { rs ->
            while (rs.next()) {
                val keywordId = rs.getLong("keyword_id").toString()
                val urlId = rs.getLong("url_id").toString()
                val term = rs.getString("term")
                table.add(arrayOf(keywordId, urlId, term))
            }
        }
        sqlite.close()
        return table.toTypedArray()
    }
}
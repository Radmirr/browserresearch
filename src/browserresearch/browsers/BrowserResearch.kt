package browserresearch.browsers

interface BrowserResearch {

    fun getBrowserName(): String
    fun getBrowserData(): List<BrowserResult>
}
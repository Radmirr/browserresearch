package browserresearch.registry

import browserresearch.registry.rejistry.BaseRejistry

class RegistryHolder {

    // for registry files
    fun getRegisty(path: String) = BaseRejistry(path)
}
package browserresearch.registry

import browserresearch.registry.rejistry.TypeOS

class ForensicRegistry(path: String) {
    private val registry: BaseRegistry = RegistryHolder().getRegisty(path)

    fun getTypeOS(): TypeOS? {
        return try {
            val name = registry.getValue(
                HKEY.LOCAL_MACHINE,
                "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion",
                "ProductName"
            ) ?: ""
            TypeOS.values().find { it.osName in name }
        } catch (e: Exception) {
            return TypeOS.WINDOWS_10
        }
    }
}


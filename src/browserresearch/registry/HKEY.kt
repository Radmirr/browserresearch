package browserresearch.registry

enum class HKEY {
    LOCAL_MACHINE,
    CURRENT_USER
}
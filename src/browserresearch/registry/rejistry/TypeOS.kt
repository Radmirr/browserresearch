package browserresearch.registry.rejistry

enum class TypeOS(
    val osName: String
) {
    WINDOWS_10("windows 10"),
    WINDOWS_8("windows 8"),
    WINDOWS_XP("windows xp")
}
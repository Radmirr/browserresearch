package browserresearch.registry.rejistry

import browserresearch.filesystem.FileSystemSeparator
import browserresearch.registry.BaseRegistry
import browserresearch.registry.HKEY
import browserresearch.registry.rejistry.rejistrylib.RegistryHiveFile
import browserresearch.registry.rejistry.rejistrylib.RegistryValueType
import browserresearch.registry.rejistry.rejistrylib.record.NKRecord
import java.io.File

class BaseRejistry(path: String): BaseRegistry {

    private val IS_TEST = false


    private val separator = FileSystemSeparator.getSeparator()

    // test constants
    private val TEST_HKLM_SOFTWARE_LOCATION = "RegistryTest${separator}HKLM_SOFTWARE"

    // real constants
    private val HKLM_SOFTWARE_LOCATION = "Windows${separator}System32${separator}config${separator}SOFTWARE"

    private val SOFTWARE = "software"

    private val hklm_software_hive: RegistryHiveFile

    init {
        if (IS_TEST) {
            hklm_software_hive =
                    RegistryHiveFile(File(path + TEST_HKLM_SOFTWARE_LOCATION))
        } else {
            hklm_software_hive =
                    RegistryHiveFile(File(path + HKLM_SOFTWARE_LOCATION))
        }
    }

    override fun getValues(hkey: HKEY, key: String): Map<String, String>? {
        val record = getRecord(hkey, key)
        val map = mutableMapOf<String, String>()
        record?.valueList?.values?.forEach {
            if (it.valueType == RegistryValueType.REG_SZ)
                map.put(it.name, it.value.asString)
        }
        return map
    }

    override fun getValue(hkey: HKEY, key: String, valueName: String): String? {
        val record = getRecord(hkey, key)
        val value = record?.valueList?.getValue(valueName)?.value
        return value?.asString
    }

    override fun getSubkeys(hkey: HKEY, key: String): List<String>? {
        val record = getRecord(hkey, key)
        val subkeys = mutableListOf<String>()
        record?.subkeyList?.subkeys?.forEach {
            subkeys.add(it.name)
        }
        return subkeys
    }

    private fun getRecord(hkey: HKEY, key: String): NKRecord? {
        val path = key.split("\\")
        when (hkey) {
            HKEY.LOCAL_MACHINE -> {
                when (path[0].toLowerCase()) {
                    SOFTWARE -> {
                        val newPath = path.subList(1, path.size)
                        return getNKRecord(hklm_software_hive.header.rootNKRecord, newPath)
                    }
                }
            }
            else -> Unit
        }
        return null
    }

    private fun getNKRecord(nkRecord: NKRecord, path: List<String>): NKRecord? {
        var tmp: NKRecord? = nkRecord
        path.forEach {
            tmp = tmp?.subkeyList?.getSubkey(it)
        }
        return tmp
    }
}
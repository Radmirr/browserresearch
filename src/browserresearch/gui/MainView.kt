package browserresearch.gui

import browserresearch.browsers.chrome.ChromeResearch
import browserresearch.browsers.opera.OperaResearch
import browserresearch.browsers.yandex.YandexBrowserResearch
import browserresearch.csv.CSVWriter
import javafx.scene.control.TextField
import tornadofx.*
import java.io.File

class MainView : View("Browser Research Tool") {

    private lateinit var tfSource: TextField
    private lateinit var tfResult: TextField

    override val root = vbox(10) {
        padding = insets(10)
        minWidth = 300.0

        vbox {
            spacing = 4.0

            label("Корень файловой системы")
            borderpane {
                center = vbox {
                    padding = insets(0,10,0,0)
                    tfSource = textfield()
                }
                right = vbox {
                    button("...") {
                        action { selectDir(tfSource) }
                    }
                }
            }
        }

        vbox {
            spacing = 4.0

            label("Результат")
            borderpane {
                center = vbox {
                    padding = insets(0,10,0,0)
                    tfResult = textfield()
                }
                right = vbox {
                    button("...") {
                        action { selectDir(tfResult) }
                    }
                }
            }
        }

        button("Начать исследование браузеров") {
            useMaxWidth = true
            action { doResearch() }
        }
    }

    private fun selectDir(tf: TextField) {
        val dir = chooseDirectory { }

        if (dir != null) {
            tf.text = dir.path
        }
    }

    private fun doResearch() {
        val src = tfSource.text
        val res = tfResult.text

        val csvWriter = CSVWriter()
        val researches = listOf(
            ChromeResearch(src),
            OperaResearch(src),
//                FirefoxResearch(src),
            YandexBrowserResearch(src)
        )
        researches.forEach {
            val name = it.getBrowserName()
            try {
                val data = it.getBrowserData()
                csvWriter.writeResult(File(res, name), data)
            } catch (e: Exception) {
                error("Ошибка при исследовании $name", e.message)
            }
        }

        information(
            title = "Сообщение",
            header = "Завершено",
            content = "Исследование браузеров завершено")
    }
}
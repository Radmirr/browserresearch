package browserresearch.csv

import browserresearch.browsers.BrowserResult
import java.io.File
import java.io.PrintWriter

class CSVWriter {

    private val separator = ";"

    fun writeResult(resultFolder: File, browsersResult: List<BrowserResult>) {
        if (!resultFolder.exists()) {
            resultFolder.mkdir()
        }
        browsersResult.forEach {
            val tableFile = File(resultFolder, it.name)
            tableFile.mkdir()
            it.tables.forEach {
                writeToCSV(File(tableFile, getCSVFileName(it.name)), it.data)
            }
        }
    }

    fun writeToCSV(file: File, data: Array<Array<String>>){
        val fileWriter = PrintWriter(file, "cp1251")
        data.forEach {
            val line = it.joinToString(separator)
            fileWriter.write(line + "\n")
        }
        fileWriter.close()
    }

    fun getCSVFileName(filename: String): String {
        return filename + ".csv"
    }
}